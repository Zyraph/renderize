call :log "---"
call :log "---Stage 2: Compiling Resources---"
call :log "%CUR_DATE% %TIME% !  Stage 2 Compilation Active."
call :log "---"

:: Stage 2 is compilation. This will put everything needed into the  Deploy folder and make it ready to put on a disc.
call :log "%CUR_DATE% %TIME% *  ---Stage 2: Compiling Resources...---"
call :log "---"
call :log "Deployment directory is located at %DEPLOY_DIR%"

:: Easy2Boot makes everything bootable. This is essential at runtime.
call :log "Compiling Easy2Boot Core Files..."
call :log "%CUR_DATE% %TIME% !  Copying files from %drive%Creation_Files\Easy2Boot to %DEPLOY_DIR%."
xcopy %drive%Creation_Files\Easy2Boot %DEPLOY_DIR% /Y /S /V /Q
call :log "%CUR_DATE% %TIME% !  Done copying Easy2Boot Core files."

:: Easy2Boot Rescue Discs and ISO files...
call :log "Compiling Rescue Discs and Installers..."
call :log "%CUR_DATE% %TIME% !  Copying files from %drive%Creation_Files\e2bcoreiso to %DEPLOY_DIR%\_ISO."
xcopy %drive%Creation_Files\e2bcoreiso %DEPLOY_DIR%_ISO /Y /S /V /Q
call :log "%CUR_DATE% %TIME% !  Done copying Easy2Boot ISO files."

:: GEGeek Utilities and Render Branding...
call :log "Compiling the Render Injector..."
call :log "%CUR_DATE% %TIME% !  Copying files from %drive%Creation_Files\Render_Injector to %DEPLOY_DIR%Render."
xcopy %drive%Creation_Files\Render_Injector %DEPLOY_DIR%Render /Y /S /V /Q /i
call :log "%CUR_DATE% %TIME% !  Done copying Render Injector files."

:: WSUS Offline Updates need to be pushed to Render...
call :log "Pushing WSUS Offline Update files..."
call :log "%CUR_DATE% %TIME% !  Copying files from %drive%Creation_Files\Windows Patches to %DEPLOY_DIR%Render."
xcopy "%drive%Creation_Files\Windows Patches" "%DEPLOY_DIR%Render\Windows Patches" /Y /S /V /Q
call :log "%CUR_DATE% %TIME% !  Done copying WSUS Offline Updates."

:: Main Build directory will be deployed through here.
call :log "Compiling Main Programs..."
xcopy %drive%Build %DEPLOY_DIR% /Y /S /V /Q
call :log "%CUR_DATE% %TIME% !  Done copying Main Program files."
call :log "Compiling Render Toolkit..."
xcopy %drive%Creation_Files\GEGeek_Toolkit %DEPLOY_DIR%Render /Y /S /V /Q
call :log "%CUR_DATE% %TIME% !  Done copying Render Toolkit files."

call :log ""
call :log "%CUR_DATE% %TIME% Stage 2 Completed Successfully"
call :log "%CUR_DATE% %TIME% !  Handing back to Renderize.bat..."

:::::::::::::::
:: FUNCTIONS ::
:::::::::::::::
:: We have to duplicate the log function since it doesn't get inherited from renderize.bat when the script is called
:log
echo:%~1 >> "%LOGPATH%\%LOGFILE%"
echo:%~1
goto :eof