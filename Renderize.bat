:: Purpose:       Renders updates and builds a deployable multi-tool for users who need a more robust software utility for installing programs.
:: Requirements:  1. Administrator access
::                2. 200 GB of storage space, as this literally builds a bootable USB Drive.
:: Author:        Raiok on Reddit, who is trying to start a business with break-fixing computers.
::                Uses some code based on TronScript, located at http://www.reddit.com/r/tronscript which was created by vocatus.
:: Version:       0.7  -  Multiple changes, check the changelog for more info! Administrator privileges are now mandatory!
::
:: Usage:         Place this batch file at the root of the drive you are attempting to use as the building medium. The best method
::                would be to use a virtual drive, as you can keep it separate from most files and create a stable workspace.
::                This script needs three base folders: Creation_Files, Build, and Deploy. You can modify these at any point within
::                the Variables area.
::
::                "Do not withold good from those who deserve it, when it is in your power to act." -p3:27
::
:: Changelog:
::                0.6.2 (5/1/2016): Administrator privileges can be used for the tool.
::                0.6.1 (3/13/2016): Deploy directory will be wiped automatically upon execution to ensure a clean build is created. This happens on each execution.
::                0.6 (3/13/2016): Modularized each stage, currently three stages (0 - prep, 1 - acquire, 2 - compile). WSUS now downloads/copies correctly, albeit manual.
::
::                0.5.1 (3/12/2016): Minimal logging which actually works, fixed GEGeek Toolkit to actually extract/compile correctly.
::                0.5 (2/14/2016): WSUS Manual Acquire added (no command switches means it must be manual), GEGeek Toolkit full extraction support
::
::                0.4 (1/10/2016): Code cleanup, works a little bit better for certain features (does not produce status of file copying, except in Ketarin, since it cannot be turned off)
::
::                0.3 (12/27/2015): Logging support! At least a little bit, anyway, it doesn't copy exact errors.
::
::                0.2 (12/20/2015): Code rehash; basic implementation of exact status and implemented a couple things from TronScript (Script version, framework for logging, etc.)
::
::                0.1 (12/7/2015): Initial build.


::::::::::::::::::::::::::::::::::::::::::::::::::::
:: SCRIPT START                                   ::
::::::::::::::::::::::::::::::::::::::::::::::::::::

:: Only experienced coders know why the next four lines are required.
@setlocal enableextensions
:: Where the **** are we and can I figure that out as an admin?
@cd /d "%~dp0"
@echo off
cls

:: VARIABLES (Do not edit without really, really good knowledge of what you are doing!)

call :set_cur_date

:: Where are we? If it's not root, I'm not going to work!
for /f "delims=" %%x in ('chdir') do set "drive=%%x"
echo Drive %drive%

:: Version
set "SCRIPT_VERSION=0.7.0"
echo Version %SCRIPT_VERSION%
:: Build Date
set "SCRIPT_DATE=2016-05-03"
echo Build Date: %SCRIPT_DATE%
:: Set titlebar to show what exactly we are loading here
title Dragon Glitch Renderize %SCRIPT_VERSION% (%SCRIPT_DATE%)
pause
:: Deploying from?
set "DEPLOY_DIR=%drive%Deploy"
echo Deploying from %DEPLOY_DIR%


:: This place has files already. Erasing to ensure integrity of build.
if exist %DEPLOY_DIR% (
echo Warning: %DEPLOY_DIR% already exists. Erasing contents for clean deployment.
RD /S /Q "%DEPLOY_DIR%"
mkdir "%DEPLOY_DIR%"
)
if not exist %DEPLOY_DIR% (
echo Making directory %DEPLOY_DIR%...
mkdir %DEPLOY_DIR%
)


:: Logs?
set LOGPATH=%DEPLOY_DIR%\BuildLog

:: Main Log
set LOGFILE=Build_%DTS%.log
:: Raw Logs
set RAW_LOGS=%LOGPATH%\Raw
:: Ketarin Builds
set KETARIN_LOGS=%RAW_LOGS%\Ketarin
:: WSUS Logs
set WSUS_LOGS=%RAW_LOGS%\WSUS


:: Make log file and directories if they don't already exist
for %%D in ("%LOGPATH%","%RAW_LOGS%","%KETARIN_LOGS%","%WSUS_LOGS%") do (
    if not exist %%D mkdir %%D
)

:: Begin generating a new log.
echo. > "%LOGPATH%\%LOGFILE%"

call :log "-------------------------------------------------------------------------------"
call :log "%CUR_DATE% %TIME%   Dragon Glitch Renderize v%SCRIPT_VERSION% (%SCRIPT_DATE%)"
call :log "                          OS: %WIN_VER% (%PROCESSOR_ARCHITECTURE%)"
call :log "                          Executing as %USERDOMAIN%\%USERNAME% on %COMPUTERNAME%"
call :log "                          Logfile: %LOGPATH%\%LOGFILE%"
call :log "                          Executing on drive %drive% with deployment in %DEPLOY_DIR%"
call :log "-------------------------------------------------------------------------------" 2>nul
call :log "%CUR_DATE% %TIME% !  Verbose forced."

:: Main Program
cls

call :log "Dragon Glitch Renderize Batch Tool"
call :log "Version %SCRIPT_VERSION% (%SCRIPT_DATE%)"
:: set /p EXTERNALDRIVE=Please set the directory for the external drive for installation:
:: echo %EXTERNALDRIVE% has been set as the directory.

:: Prepare link to Stage 0 file
call :log ""
call :log "Activating Stage 0 - Preparation..."
call stage_0.bat

:: After Handing back, do this:
call :log ""
call :log "Activating Stage 1 - Acquisition..."
call stage_1.bat

:: After Handing back, do this:
call :log ""
call :log "Activating Stage 2 - Compilation..."
call stage_2.bat

:: End of stages, finalizing log
call :log "Compilation Complete. All utilities are ready for deployment."
call :log "-------------------------------------------------------------------------------"
call :log "%CUR_DATE% %TIME%   Dragon Glitch Renderize v%SCRIPT_VERSION% (%SCRIPT_DATE%)"
call :log "                          OS: %WIN_VER% (%PROCESSOR_ARCHITECTURE%)"
call :log "                          Executing as %USERDOMAIN%\%USERNAME% on %COMPUTERNAME%"
call :log "                          Executed on drive %drive% with deployment in %DEPLOY_DIR%"
call :log "                          Logfile: %LOGPATH%%LOGFILE%"
call :log ""
call :log "     Time to test it! ;3"
call :log "-------------------------------------------------------------------------------"
ENDLOCAL

:::::::::::::::
:: FUNCTIONS ::
:::::::::::::::
:: Thanks to /u/douglas_swehla for helping here (through TronScript)
:: Since no new variable names are defined, there's no need for SETLOCAL.
:: The %1 reference contains the first argument passed to the function. When the
:: whole argument string is wrapped in double quotes, it is sent as an argument.
:: The tilde syntax (%~1) removes the double quotes around the argument.
:log
echo:%~1 >> "%LOGPATH%\%LOGFILE%"
echo:%~1
goto :eof

:: Get the date into ISO 8601 standard format (yyyy-mm-dd) so we can use it
:set_cur_date
for /f %%a in ('WMIC OS GET LocalDateTime ^| find "."') DO set DTS=%%a
set CUR_DATE=%DTS:~0,4%-%DTS:~4,2%-%DTS:~6,2%
goto :eof

:eof