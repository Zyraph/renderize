call :log "---"
:: Stage 0 is always preparing files. This is strictly for the latest version of the GEGeek Toolkit, which cross-compiles into Renderize and Render.
call :log "---Stage 0: Preparing Files---"
call :log "%CUR_DATE% %TIME% !  Stage 0 Preparation Active."
call :log "---"
call :log "Checking for GEGeek Toolkit Updates..."
:: This one line defines if there's an update to GEGeek or not. Naming it "Tech_Toolkit.zip" in the directory specified will trigger this.
IF EXIST %drive%Creation_Files\Tech_Toolkit.zip (
:: Call the updater
call :gegeekupdate
)
call :log "%CUR_DATE% %TIME% GEGeek Toolkit update check complete."
call :log ""
call :log "%CUR_DATE% %TIME% Stage 0 Completed Successfully"
call :log "%CUR_DATE% %TIME% !  Handing back to Renderize.bat..."


:::::::::::::::
:: FUNCTIONS ::
:::::::::::::::
:: We have to duplicate the log function since it doesn't get inherited from renderize.bat when the script is called

:log
echo:%~1 >> "%LOGPATH%\%LOGFILE%"
echo:%~1
goto :eof

:: This function serves to update the core GEGeek files which runs the main functions of the entire toolkit. This is the only way to currently do it automatically.
:gegeekupdate
call :log "%CUR_DATE% %TIME% GEGeek Toolkit appears to have an update. Updating..."
:: We must delete the pervious files for the toolkit to proceed.
IF EXIST %drive%Creation_Files\GEGeek_Toolkit (rmdir %drive%Creation_Files\GEGeek_Toolkit /s /q)
:: Move into the directory to actually decompress and move the update there
pushd %drive%Creation_Files
7z x %drive%Creation_Files\Tech_Toolkit.zip
:: Get out of that directory and move back to standard building directory.
popd
:: This line deletes the zip file containing the updates. Comment it out if you don't like/want that.
del %drive%Creation_Files\Tech_Toolkit.zip
:: This file holds the main updates for all GEGeek Toolkit files through Ketarin. We copy this into our own Ketarin downloader.
xcopy %drive%Creation_Files\GEGeek_Toolkit\gegeek_apps %drive%Creation_Files\Ketarin\Renderize /y /q
call :log "%CUR_DATE% %TIME% GEGeek Toolkit has been updated to the latest version."
goto :eof

:eof