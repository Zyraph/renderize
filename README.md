Welcome to the Renderize Git Repo!

For best results, please note that this repo uses both **HTTPS** and **SSH** access.


# Synopsis #

Renderize is a modular batch file used to create a deployable external drive that can help troubleshoot, diagnose, and fix computers suffering from a multitude of issues. **NOTE:** This tool is still extremely early in development and features/functions can change. Use with extreme caution!

# Code Example #

Renderize uses the GEGeek Toolkit (located at [http://gegeek.com](http://gegeek.com)) for its backbone. GEGeek is an advanced updater for multiple programs and files that are to help experienced computer technicians with solving multiple computer issues. To do this, Renderize provides an automated updater for all GEGeek files, as referenced here:


```
#!batch
:gegeekupdate
call :log "%CUR_DATE% %TIME% GEGeek Toolkit appears to have an update. Updating..."
:: We must delete the pervious files for the toolkit to proceed.
IF EXIST %drive%Creation_Files\GEGeek_Toolkit (rmdir %drive%Creation_Files\GEGeek_Toolkit /s /q)
:: Move into the directory to actually decompress and move the update there
pushd %drive%Creation_Files
7z x %drive%Creation_Files\Tech_Toolkit.zip
:: Get out of that directory and move back to standard building directory.
popd
:: This line deletes the zip file containing the updates. Comment it out if you don't like/want that.
del %drive%Creation_Files\Tech_Toolkit.zip
:: This file holds the main updates for all GEGeek Toolkit files through Ketarin. We copy this into our own Ketarin downloader.
xcopy %drive%Creation_Files\GEGeek_Toolkit\gegeek_apps %drive%Creation_Files\Ketarin\Renderize /y /q
call :log "%CUR_DATE% %TIME% GEGeek Toolkit has been updated to the latest version."
goto :eof

```


# Motivation #

I wanted to make something that would automatically update GEGeek and allow other tools to be implemented into its own multitool. I also wanted to have more control over what others saw within the Toolkit, to ensure those that don't know what they are doing wouldn't start looking directly into the programs involved and accidentally make troubleshooting worse, or try doing things themselves after everything is fixed. I figured I could combine both pieces into one automated program that would ensure things would work correctly while keeping some of the tools more hidden away. Of course, credit will be given where it is due, but not everyone on the planet needs to know everything that is used to make their system stable again, especially since these tools can easily destroy computers if used improperly.

# Installation #

**Requirements:**
Administrator privileges are required as of version 0.7 since multiple drives are accessed and one registry key is created (and deleted) while the program is in use.
A minimum of 200 GB of space, preferably on a virtual or isolated drive (we are building a multitool, and it will write a lot of data at once)
A decent internet connection will help significantly since we are downloading multiple programs as well as updating them. 1mbps or greater is encouraged, but it can be used on slower connections if you are willing to leave your computer running for hours on end.
Windows 7 x64 is the only operating system currently supported. It has not been tested under any other environment as of yet.

1. Place these files in the root directory of the drive you are using for staging (not the External drive, if you'll be using it for repair). A virtual drive with a minimum of 200 GB is encouraged.
2. Run the Renderize Preparation Tool (renprep.bat). You will see warnings. Accept/ignore at your leisure.
3. ???
4. Profit!

# API Reference #
No API is available at this time.

# Tests #

A more concrete way to test this will be written once 0.7 is released.

# Contributors #

This is currently a solo development project. More will be released after some of the basics are covered.

# License #

Current license is under the MIT license. 

# Special Thanks #

Special thanks goes to GEGeek, for making possibly the best collection of tools for any serious computer technician. [Get your copy of GEGeek Toolkit over here!](http://gegeek.com)

[TronScript](http://www.reddit.com/r/tronscript), created by u/vocatus, has a very extensive use for malware removal and many, MANY other features. I may have borrowed a little bit of the thousands of lines of code from it. It's also teaching me just how batch works in its own way!

[Ketarin](http://ketarin.canneverbe.com/) is a program updater that can be customized for many different programs. Both Renderize and GEGeek Toolkit use this to keep everything up to date! Check it out, since it's getting even better!

[Easy2Boot](http://www.easy2boot.com/) is a special bootloader for DVDs and External Drives. You can load many different operating systems within it, including almost anything Windows, to quite a few flavors of Linux. You can also load customized versions of these operating systems as long as most of the main files are intact and it is bootable. This is the other half of what makes Renderize function!