call :log "---"
:: Stage 1 is always updating files. This will ensure the latest versions are always available at the time of building.
call :log "---Stage 1: Acquiring Files---"
call :log "%CUR_DATE% %TIME% !  Stage 1 Acquisition Active."
call :log "---"

:: Start Core Updating. This includes all rescue discs.
call :log "%CUR_DATE% %TIME% Starting Ketarin Core Updater..."
call :log "Current drive is %drive%"
call :log "%CUR_DATE% %TIME% !  Ketarin is running from %drive%Creation_Files\Ketarin\Ketarin.exe with database Renderize_Core."
pushd Creation_Files\Ketarin\
call Ketarin.exe /database="Renderize_Core" /silent /notify /log=%KETARIN_LOGS%\Core_Creation.log
call :log "%CUR_DATE% %TIME% !  Ketarin's Renderize_Core is done updating. Log located at %KETARIN_LOGS%Core_Creation.log"

:: Start Main Updates. This is through GEGeek's toolkits.
call :log "Starting Ketarin Renderize Updater...
call :log "%CUR_DATE% %TIME% !  Ketarin is running from %drive%Creation_Files\Ketarin\Ketarin.exe with database Renderize."
call Ketarin.exe /database="Renderize" /silent /log=%KETARIN_LOGS%\Renderize.log
call :log "Ketarin Renderize Updater Complete. Log located at %KETARIN_LOGS%\Renderize.log"
popd

:: Prompt for WSUS Offline Update to update things (Cannot be automated since no command line switches are used for updating)
call :log "Launching WSUS Offline Update..."
call :log "%CUR_DATE% %TIME% !  WSUS Offline Update is running from %drive%Creation_Files\wsusoffline\UpdateGenerator.exe with a built-in database."
start /wait %drive%Creation_Files\wsusoffline\UpdateGenerator.exe
xcopy %drive%Creation_Files\wsusoffline\log\download.log %WSUS_LOGS%logfile.txt /Y /S /V /Q
call :log "WSUS Offline Update Complete. Log located at %WSUS_LOGS%\logfile.txt"
call :log "---"
call :log ""
call :log "%CUR_DATE% %TIME% Stage 1 Completed Successfully"
call :log "%CUR_DATE% %TIME% !  Handing back to Renderize.bat..."


:::::::::::::::
:: FUNCTIONS ::
:::::::::::::::
:: We have to duplicate the log function since it doesn't get inherited from renderize.bat when the script is called
:log
echo:%~1 >> "%LOGPATH%\%LOGFILE%"
echo:%~1
goto :eof